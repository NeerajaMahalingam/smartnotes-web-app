# SmartNotes App

This is an advanced version of default notes application used in mobile phones. It allows the users to share the list/to-do items with their contacts in Facebook. 
This application uses FB-SDK for login user authentication & loading friends details for notes sharing. User can edit/view/delete the items in the notes. 
The use-case is to allow people to share the notes instead of messaging or mailing.
Front End  - User Authorization - FB SDk,JavaScript,HTML5
Client Server communication - ReSTful services implemeted using Spring Framework
Back-end Database - MongoDB
 