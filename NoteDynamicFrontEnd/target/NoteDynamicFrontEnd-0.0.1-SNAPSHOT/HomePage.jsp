<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="javax.servlet.http.HttpSession"%>
<%@page import="java.util.List"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="NotePageDesign.css">

<title>Home Page</title>
</head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<body>
	<jsp:include page="header.jsp">
		<jsp:param name="UserName"
			value="<%=request.getParameter(\"username\")%>" />
	</jsp:include>
	<%
		HttpSession sessionName = request.getSession();
		System.out.println("UserName outside:--" + request.getParameter("username"));
		System.out.println("Useremail outside:--" + request.getParameter("email"));
		String loggedUserName = "";
		/*if (request.getAttribute("username") != null) {
		loggedUserName = request.getAttribute("username").toString();
		System.out.println("UserName:--"+ loggedUserName);
		}*/
		if (request.getParameter("username") != null) {
			loggedUserName = request.getParameter("username").toString();
			System.out.println("UserName in get Param:--" + loggedUserName);
			sessionName.setAttribute("userHeader", loggedUserName);
		}

		String loggedUserEmail = "";
		if (request.getParameter("email") != null) {
			loggedUserEmail = request.getParameter("email").toString();
			System.out.println("UserEmail inside param:--" + loggedUserEmail);
		}
		/*List<String> test_str = (List<String>) request.getAttribute("noteNames");
		System.out.println("inside JSP store names***"+test_str.get(0));*/
	%>
	<br>
	<br>
	<form>
		<table id="tweet" align="justify">
			<tr>
				<th align="left">List Name</th>
				<th align="left">Delete</th>
			</tr>
			<c:forEach items="${noteNames}" var="nn">
				<tr>
						<td><span>${nn.name}</span></td>
						<td><input type="button" value="Delete" id="DeleteButton"
							width=50% onclick="form.action='DeletePage.jsp';form.submit();">
						</td>
				</tr>

			</c:forEach>
			<tr>
				<td><input type="button" value="Create New" id="CreateNew"
					width=50% onclick="form.action='NewNotePage.jsp';form.submit();">
			</tr>
		</table>
	</form>
</body>
</html>