package notesPackage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class CreateServlet
 */
@WebServlet("/CreateServlet")
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Inside createServlet");
		String createName = request.getParameter("ListName");
		List<String> userEmail = new ArrayList<String>();
		userEmail.add(request.getSession().getAttribute("userEmail").toString());
		NoteBusinessLogic bl = new NoteBusinessLogic();
		String noteId = bl.createNewNote(createName,userEmail); 
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
		request.setAttribute("noteName", createName);
		request.setAttribute("noteId", noteId);
		String url = "/AddItemPage.jsp";   // the "Home" page
        // forward request and response objects to specified URL
        getServletContext().getRequestDispatcher(url).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
