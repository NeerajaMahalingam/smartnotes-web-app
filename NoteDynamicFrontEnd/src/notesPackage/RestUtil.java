package notesPackage;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public final class RestUtil {
	final HttpHeaders requestHeaders = new HttpHeaders();
    public RestUtil(){
    	initHeader();
    }
	public void initHeader() {
		requestHeaders.add("Content-Type", "application/json");
		requestHeaders.add("Accept", "application/json");
	}

	public String createNewNote(String ListName, List<String> UserEmail) {
		final String postURL = "http://localhost:8080/note";
		JSONObject noteJSON = new JSONObject();
		JSONArray noteArrayJSON = new JSONArray();
		for(String x:UserEmail) {
			noteArrayJSON.put(x);
		}
		noteJSON.put("name", ListName);
		noteJSON.put("email", noteArrayJSON);
		ResponseEntity<String> response = ExecutePostRequest(postURL, noteJSON.toString());
		if (response == null) {
			System.out.println("Rest API to create note failed!!!!");
		} else {
			System.out.println("noteId value" + response.getBody());
			return response.getBody();
		}
		return null;
	}

	public ResponseEntity<String> ExecutePostRequest(String url, String propertiesToSet) {
		RestTemplate restClient = new RestTemplate();
		HttpEntity<String> requestEntity;
		requestEntity = new HttpEntity<String>(propertiesToSet, requestHeaders);
		final ResponseEntity<String> output = restClient.exchange(url, HttpMethod.POST, requestEntity, String.class);
		final HttpStatus statusCode = output.getStatusCode();
		return output;
	}

	public final List<Note> getListNames(String UserEmail) {
		final String postURL = "http://localhost:8080/notes?email="+UserEmail;
		ResponseEntity<String> response = ExecuteGetRequest(postURL, requestHeaders);
		List<Note> retListName = new ArrayList<Note>();
		if (response == null) {
			System.out.println("Rest API to create note failed!!!!");
		} else {
			//JSONObject retJSON = new JSONObject(response.getBody());
			JSONArray retJSONArr = new JSONArray(response.getBody());
			for (int i = 0; i < retJSONArr.length(); i++) {
				Note tempNote = new Note();
				tempNote.setNoteId(retJSONArr.getJSONObject(i).getString("noteId"));
				tempNote.setName(retJSONArr.getJSONObject(i).getString("name"));
				
				JSONArray emailArr = new JSONArray(retJSONArr.getJSONObject(i).get("email").toString());
				List<String> emails = new ArrayList<String>();
				for (int j = 0; j < emailArr.length(); j++) {
					emails.add(emailArr.getString(j));
				}
				tempNote.setEmail(emails);
				retListName.add(tempNote);
			}
		}
		return retListName;
	}
	
	public final Note getNoteDetails(String noteId) {
		final String postURL = "http://localhost:8080/note?noteId="+noteId;
		ResponseEntity<String> response = ExecuteGetRequest(postURL, requestHeaders);
		JSONObject retJson = new JSONObject(response.getBody());
		Note noteDet = new Note();
		noteDet.setNoteId(retJson.getString("noteId"));
		noteDet.setName(retJson.getString("name"));
		
		if (retJson.get("email")!= null) {
			JSONArray emailArr = new JSONArray(retJson.get("email").toString());
			List<String> emails = new ArrayList<String>();
			for (int i = 0; i < emailArr.length(); i++) {
				emails.add(emailArr.getString(i));
			}
			noteDet.setEmail(emails);
		}
		
		if (retJson.get("item")!= null) {
			JSONArray itemArr = new JSONArray(retJson.get("item").toString());
			List<String> items = new ArrayList<String>();
			for (int i = 0; i < itemArr.length(); i++) {
				items.add(itemArr.getString(i));
			}
			noteDet.setItem(items);
		}
		
		return noteDet;
	}

	public ResponseEntity<String> ExecuteGetRequest(String url, HttpHeaders requestHeaders) {
		RestTemplate restClient = new RestTemplate();
		HttpEntity requestEntity;
		requestEntity = new HttpEntity(null,requestHeaders);
		final ResponseEntity<String> output = restClient.exchange(url, HttpMethod.GET, requestEntity, String.class);
		final HttpStatus statusCode = output.getStatusCode();
		return output;
	}
	public void updateName(String NoteId,String UserName){
		final String postURL = "http://localhost:8080/note/"+NoteId;
		JSONObject noteJSON = new JSONObject();
		noteJSON.put("name", UserName);
		ResponseEntity<String> response = ExecutePutRequest(postURL, noteJSON.toString());
		if (response == null) {
			System.out.println("Rest API to Update note failed!!!!");
		} else {
			System.out.println("noteId value:"+response.getBody());
		}
		
	}
	/*update item same put request*/
	public void updateItem(String NoteId,String Item){
		final String postURL = "http://localhost:8080/note/"+NoteId+"/item";
		JSONObject noteJSON = new JSONObject();
		//noteJSON.put("noteId", NoteId);
		noteJSON.put("item", Item);
		ResponseEntity<String> response = ExecutePutRequest(postURL, noteJSON.toString());
		if (response == null) {
			System.out.println("Rest API to Update note failed!!!!");
		} else {
			System.out.println("noteId value:"+response.getBody());
		}
		
	}
	/*Same put request for update name & update item*/
	public ResponseEntity<String> ExecutePutRequest(String url, String propertiesToSet) {
		RestTemplate restClient = new RestTemplate();
		HttpEntity<String> requestEntity;
		requestEntity = new HttpEntity<String>(propertiesToSet, requestHeaders);
		final ResponseEntity<String> output = restClient.exchange(url, HttpMethod.PUT, requestEntity, String.class);
		final HttpStatus statusCode = output.getStatusCode();
		return output;
	}
	public void deleteNote(String NoteId) {
		final String postURL = "http://localhost:8080/note/"+NoteId;
		ResponseEntity<String> response = ExecuteDeleteRequest(postURL, requestHeaders);
		if (response == null) {
			System.out.println("Rest API to create note failed!!!!");
		}
	}
	
	public Note deleteItem(String NoteId,String Item) {
		final String postURL = "http://localhost:8080/note/"+NoteId+"/deleteitem";
		JSONObject noteJSON = new JSONObject();
		//noteJSON.put("noteId", NoteId);
		noteJSON.put("item", Item);
		ResponseEntity<String> response = ExecutePutRequest(postURL, noteJSON.toString());
		JSONObject retJson = new JSONObject(response.getBody());
		Note noteDet = new Note();
		if (retJson.get("item")!= null) {
			JSONArray itemArr = new JSONArray(retJson.get("item").toString());
			List<String> items = new ArrayList<String>();
			for (int i = 0; i < itemArr.length(); i++) {
				items.add(itemArr.getString(i));
			}
			noteDet.setItem(items);
		}
		return noteDet;
	}
	
	public ResponseEntity<String> ExecuteDeleteRequest(String url, HttpHeaders requestHeaders) {
		RestTemplate restClient = new RestTemplate();
		HttpEntity requestEntity;
		requestEntity = new HttpEntity(null,requestHeaders);
		final ResponseEntity<String> output = restClient.exchange(url, HttpMethod.DELETE, requestEntity, String.class);
		final HttpStatus statusCode = output.getStatusCode();
		return output;
	}
	/*creating new element in people collection*/
	
	public String createNewPeople(String UserName, String UserEmail) {
		final String postURL = "http://localhost:8080/people";
		JSONObject noteJSON = new JSONObject();
		noteJSON.put("name",UserName);
		noteJSON.put("email",UserEmail);
		ResponseEntity<String> response = ExecutePostRequest(postURL, noteJSON.toString());
		if (response == null) {
			System.out.println("Rest API to create note failed!!!!");
		} else {
			System.out.println("PeopleId value" + response.getBody());
			return response.getBody();
		}
		return null;
	}
	
	public List<String> getAllPeople() {
		final String postURL = "http://localhost:8080/people/all";
		ResponseEntity<String> response = ExecuteGetRequest(postURL, null);
		
		List<String> peopleEmails = new ArrayList<String>();
		JSONArray retJSONArr = new JSONArray(response.getBody());
		for (int i = 0; i < retJSONArr.length(); i++) {
			peopleEmails.add(retJSONArr.getJSONObject(i).getString("email"));
		}
		return peopleEmails;
	}
}

