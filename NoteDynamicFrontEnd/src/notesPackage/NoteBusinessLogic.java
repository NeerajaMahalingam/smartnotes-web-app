package notesPackage;
import java.util.List;

public class NoteBusinessLogic {
	RestUtil restUtils;

	public NoteBusinessLogic() {
		restUtils = new RestUtil();
	}

	public List<Note> getListNames(String email) {
		return restUtils.getListNames(email);
	}

	public String createNewNote(String ListName, List<String> UserEmail) {

		return restUtils.createNewNote(ListName, UserEmail);
	}

	public void updateName(String NoteId, String UserName) {

		restUtils.updateName(NoteId, UserName);
	}

	public void deleteNote(String NoteId) {

		restUtils.deleteNote(NoteId);
	}
	public void updateItem(String NoteId, String item){
		restUtils.updateItem(NoteId,item);
	}
	public Note getNoteDetails(String NoteId){
		return restUtils.getNoteDetails(NoteId);
	}
	
	public Note deleteItem(String NoteId,String Item){
		return restUtils.deleteItem(NoteId,Item);
	}
	
	public String createNewPeople(String UserName, String UserEmail) {

		return restUtils.createNewPeople(UserName, UserEmail);
	}
	
	public List<String> getAllPeople() {

		return restUtils.getAllPeople();
	}
	
}
