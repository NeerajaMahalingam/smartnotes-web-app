package notesPackage;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DelItemServlet
 */
@WebServlet("/DelItemServlet")
public class DelItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DelItemServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String noteId = request.getParameter("noteIdHid");
		String itemName = request.getParameter("delItem");
		
		
		NoteBusinessLogic bl = new NoteBusinessLogic();
		bl.deleteItem(noteId, itemName);
		
      Note noteDet = bl.getNoteDetails(noteId);
		
		request.setAttribute("allItems", noteDet.getItem());
		request.setAttribute("noteName", noteDet.getName());
		request.setAttribute("noteId", noteDet.getNoteId());
		
		String url = "/AddItemPage.jsp";   // the "Home" page
        // forward request and response objects to specified URL
        getServletContext().getRequestDispatcher(url).forward(request, response);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
