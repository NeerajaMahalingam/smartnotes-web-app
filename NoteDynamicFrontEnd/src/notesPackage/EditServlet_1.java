package notesPackage;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class EditServlet_1
 */
@WebServlet("/EditServlet_1")
public class EditServlet_1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditServlet_1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String noteId = request.getParameter("noteId");
		System.out.println("Iniside edit:"+noteId);
		NoteBusinessLogic bl = new NoteBusinessLogic();
		List<Note> noteNames = bl.getListNames(request.getSession().getAttribute("userEmail").toString());
		for(Note x : noteNames) {
			if(x.getNoteId().equals(noteId)) {
				String name = x.getName();
				 request.setAttribute("modifyName", name);
				 request.setAttribute("noteId", noteId);
				 request.setAttribute("sharedWith",x.getEmail());
			}
		}
		List<String> allPeopleEmails = bl.getAllPeople();
		request.setAttribute("peopleEmails", allPeopleEmails);
		
		String url = "/EditNotePage.jsp";   // the "Home" page
        // forward request and response objects to specified URL
        getServletContext().getRequestDispatcher(url).forward(request, response);		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
