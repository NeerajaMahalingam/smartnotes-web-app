package notesPackage;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Servlet implementation class HomeServlet
 */
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		NoteBusinessLogic bl = new NoteBusinessLogic();
		HttpSession session = request.getSession();
		
		if(session.getAttribute("userHeader") == null) {
			session.setAttribute( "userHeader", request.getParameter("userName").toString());
			session.setAttribute( "userEmail", request.getParameter("userEmail").toString());
			bl.createNewPeople(session.getAttribute("userHeader").toString(), session.getAttribute("userEmail").toString());	
		}
		
		List<Note> noteNames = bl.getListNames(session.getAttribute("userEmail").toString());
		request.setAttribute("noteNames", noteNames);
		String url = "/HomePage.jsp";   // the "Home" page
        // forward request and response objects to specified URL
        getServletContext().getRequestDispatcher(url).forward(request, response);
       
		response.getWriter().append("Served at: ").append(request.getContextPath());		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
