<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="javax.servlet.http.HttpSession"%>
<%@page import="java.util.List"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="NotePageDesign.css">

<title>Home Page</title>
</head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<body class="bodycss">
	<header> <jsp:include page="header.jsp"></jsp:include> </header>
	<div class="content">
		<%
			HttpSession sessionName = request.getSession();
			String loggedUserName = "";
			if (request.getParameter("username") != null) {
				loggedUserName = request.getParameter("username").toString();
				sessionName.setAttribute("userHeader", loggedUserName);
			}

			String loggedUserEmail = "";
			if (request.getParameter("email") != null) {
				loggedUserEmail = request.getParameter("email").toString();
			}
			/*List<String> test_str = (List<String>) request.getAttribute("noteNames");
			System.out.println("inside JSP store names***"+test_str.get(0));*/
		%>
		<br> <br>
		<form>
			<table id="tweet" align="justify">
				<tr>
					<th align="left">List Name</th>
					<th align="left">Edit</th>
					<th align="left">Delete</th>
				</tr>
				<c:forEach items="${noteNames}" var="nn">
					<tr>
						<form>
							<td><input type="button" id="ListNameButton"
								onclick="form.action='AddItemServlet'; form.submit();"
								class="btn2" value="<c:out value="${nn.name}"/>" /> <input
								type="hidden" name="noteId" id="noteId"
								value="<c:out value="${nn.noteId}"/>" /></td>
							<td><input type="button" value="Edit" id="EditButton"
								width=50% onclick="form.action='EditServlet_1'; form.submit();">
							</td>
							<td><input type="button" value="Delete" id="DeleteButton"
								width=50% onclick="form.action='DeleteServlet'; form.submit();">
							</td>
						</form>
					</tr>
				</c:forEach>
			</table>
		</form>
		<table>
			<tr></tr>
			<tr>
				<td><input class="btn1" type="button" value="Create New"
					id="CreateNew" width=50%
					onclick="form.action='NewNotePage.jsp';form.submit();"></td>
			</tr>
		</table>
	</div>
	<div class="spacer"></div>
	<footer class="footer"> <jsp:include page="footer.jsp"></jsp:include>
	</footer>
</body>
</html>