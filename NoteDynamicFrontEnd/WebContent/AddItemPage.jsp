<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="NotePageDesign.css">
<link rel="stylesheet" href="NoteItemDesign.css">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<title>Add Item Page</title>
<script>
	function updateList() {
		//Create List
		//For loop 
		//var li = document.createElement("li");
		//var inputValue = //Request.getAttribute
		//var t = document.createTextNode(inputValue);
		//li.appendChild(t);
		//sdocument.getElementById("myUL").appendChild(li);
		//End of or loop

		//Create a "close" button and append it to each list item
		var myNodelist = document.getElementsByTagName("LI");
		var i;
		for (i = 0; i < myNodelist.length; i++) {
			var span = document.createElement("SPAN");
			var txt = document.createTextNode("\u00D7");
			span.className = "close";
			span.appendChild(txt);
			myNodelist[i].appendChild(span);
		}

		// Click on a close button to hide the current list item
		var close = document.getElementsByClassName("close");
		var i;
		for (i = 0; i < close.length; i++) {
			close[i].onclick = function() {
				var div = this.parentElement;
				console.log(div.childNodes);
				var myForm = document.getElementById('theForm');
				document.getElementById('delItem').value = div.childNodes[0].innerHTML;
				console.log(div.childNodes[0].innerHTML);
				//div.style.display = "none";
				myForm.submit(this);
			}
		}

		// Add a "checked" symbol when clicking on a list item
		var list = document.querySelector('ul');
		list.addEventListener('click', function(ev) {
			if (ev.target.tagName === 'LI') {
				ev.target.classList.toggle('checked');
			}
		}, false);
	}

	// Create a new list item when clicking on the "Add" button
	function newElement() {
		var li = document.createElement("li");
		var inputValue = document.getElementById("myInput").value;
		var t = document.createTextNode(inputValue);
		li.appendChild(t);
		if (inputValue === '') {
			alert("You must write something!");
		} else {
			document.getElementById("myUL").appendChild(li);
		}
		document.getElementById("myInput").value = "";

		var span = document.createElement("SPAN");
		var txt = document.createTextNode("\u00D7");
		span.className = "close";
		span.appendChild(txt);
		li.appendChild(span);

		for (i = 0; i < close.length; i++) {
			close[i].onclick = function() {
				var div = this.parentElement;
				div.style.display = "none";
			}
		}
	}
</script>
</head>
<body onload="updateList();" class="bodycss">
	<jsp:include page="header.jsp"></jsp:include>
	<div class="content">
		<div id="myDIV" class="header">
			<h2 style="margin: 5px"><%=request.getAttribute("noteName").toString()%></h2>
			<form>
			<table align ="center">
			<tr>
			<td>
			<input type="hidden" name="noteId" id="noteId"
					value=<%=request.getAttribute("noteId").toString()%> /> 
			</td>
			<td><input type="text" id="myInput" class="div2" placeholder="Title..." name="myInput" /></td>
			<br>
			<td><input type="button" value="Add" id="AddItem"
					onclick="form.action='AddItemServlet';form.submit();"
					class="btn1" /></td>
			</tr>
			</table>
					<table></table>
			</form>
		</div>
		
		<ul id="myUL">
		<h3>List Items</h3>
			<%
				if (request.getAttribute("allItems") != null) {
			%>

			<c:forEach items="${allItems}" var="nn">
				<li><label id="lblItem">${nn}</label></li>
			</c:forEach>
			<%
				}
			%>
		</ul>
	</div>
	<form id="theForm" action="DelItemServlet">
		<input type="hidden" name="noteIdHid" id="noteIdHid"
			value=<%=request.getAttribute("noteId").toString()%> /> <input
			type="hidden" name="delItem" id="delItem" /> <br> <input
			class="btn1" type="button" value="Done" id="Done" width=50%
			onclick="form.action='HomeServlet';form.submit();" />
	</form>
	</div>
	<div class="spacer"></div>
	<footer class="footer"> <jsp:include page="footer.jsp"></jsp:include>
	</footer>
</body>
</html>