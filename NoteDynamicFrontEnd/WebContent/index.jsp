<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login Page</title>
<script src="jquery-3.2.1.js"></script>
<script>
	window.fbAsyncInit = function() {
		FB.init({
			appId : '153658568578232',
			xfbml : true,
			version : 'v2.1'
		});

		function onLogin(response) {
			if (response.status == 'connected') {
				FB
						.api(
								'/me?fields=name,email',
								function(data) {

									var welcomeBlock = document
											.getElementById('Notes-welcome');
									welcomeBlock.innerHTML = 'Welcome, '
											+ data.name + '!';
									console.log(data);

									var myForm = document
											.getElementById('theForm');
									document.getElementById('userName').value = data.name;
									document.getElementById('userEmail').value = data.email;
									myForm.submit(this);

								});
			} else {
				var welcomeBlock = document.getElementById('Notes-welcome');

				welcomeBlock.innerHTML = 'Cant get data ' + response.status
						+ '!';
			}
		}

		FB.getLoginStatus(function(response) {
			// Check login status on load, and if the user is
			// already logged in, go directly to the welcome message.
			if (response.status == 'connected') {
				onLogin(response);
			} else {
				// Otherwise, show Login dialog first.
				FB.login(function(response) {
					onLogin(response);
				}, {
					scope : 'user_friends, email'
				});
			}
		});
	};

	function checkLoginState() {
		FB
				.login(function(response) {
					if (response.status === "connected") {
						document.getElementById("status").innerHTML = "we are connected.";
						onLogin(response);
					} else {
						document.getElementById("status").innerHTML = "not connected.";
					}
				});
	}

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {
			return;
		}
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<h2>Click here to login using Facebook</h2>
	<br>
	<fb:login-button scope="public_profile, email"
		onlogin="checkLoginState();">
	</fb:login-button>

	<h1 id="Notes-welcome"></h1>

	<form id="theForm" action="HomeServlet">
		<input type="hidden" name="userName" id="userName" /> <input
			type="hidden" name="userEmail" id="userEmail" />
	</form>
</body>
</html>