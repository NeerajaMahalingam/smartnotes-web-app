<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="NotePageDesign.css">
<title>Header Page</title>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

</head>
<body>
	<div class="topnav" id="myTopnav">

		<h1 style="text-align: center">Welcome to Smart Notes Application
		</h1>
		<%if(request.getSession().getAttribute("userHeader") != null) 
       {%>
		<h3 style="float: right"><%= request.getSession().getAttribute("userHeader")%></h3>
		<%} else{%>
		<h3 style="float: right">SignIn</h3>
		<% }%>
		<a href="<%=request.getContextPath()%>/HomeServlet">Home</a>
	</div>
</body>
</html>