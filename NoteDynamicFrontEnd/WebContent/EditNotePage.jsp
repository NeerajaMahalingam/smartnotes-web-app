<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<title>Edit Note Page</title>
</head>
<body class="bodycss">
	<jsp:include page="header.jsp"></jsp:include>
	<div class="content">
		<h4>
			<label align="center" id="EnterName" value="Enter List Name">Enter
				List Name</label>
		</h4>
		<form>
			<table id="texttable" align="justify">
				<table>
					<tr>
						<td class="div1"><input class="div1" type="text"
							name="updateName"
							value="<%=request.getAttribute("modifyName").toString()%>"></td>
						<td><input type="hidden" name="noteId" id="noteId"
							value="<%=request.getParameter("noteId")%>" /></td>
						<td><input class="btn1" type="button" value="Save"
							id="EditName" width=50%
							onclick="form.action='EditServlet_2';form.submit();"></td>
					</tr>
				</table>
			</table>
		</form>
		<br>
		<form>
			<h2>List members</h2>
			<table id="tweet_1" align="justify">
				<tr>
					<th align="left">User Name</th>
				</tr>
				<c:forEach items="${sharedWith}" var="user">
					<tr>
						<td><input class="btn2" type="button" name="printuserName"
							value="<c:out value="${user}"/>"></td>
					</tr>
				</c:forEach>
			</table>
		</form>
	</div>
	<div class="spacer"></div>
	<footer class="footer"> <jsp:include page="footer.jsp"></jsp:include>
	</footer>
</body>
</html>