package blPackage;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public final class RestUtil {
	final HttpHeaders requestHeaders = new HttpHeaders();
    public RestUtil(){
    	initHeader();
    }
	public void initHeader() {
		requestHeaders.add("Content-Type", "application/json");
		requestHeaders.add("Accept", "application/json");
	}

	public String createNewNote(String ListName, List<String> UserEmail) {
		final String postURL = "http://localhost:8080/note";
		JSONObject noteJSON = new JSONObject();
		JSONArray noteArrayJSON = new JSONArray();
		for(String x:UserEmail) {
			noteArrayJSON.put(x);
		}
		noteJSON.put("name", ListName);
		noteJSON.put("email", noteArrayJSON);
		ResponseEntity<String> response = ExecutePostRequest(postURL, noteJSON.toString());
		if (response == null) {
			System.out.println("Rest API to create note failed!!!!");
		} else {
			System.out.println("noteId value" + response.getBody());
			return response.getBody();
		}
		return null;
	}

	public ResponseEntity<String> ExecutePostRequest(String url, String propertiesToSet) {
		RestTemplate restClient = new RestTemplate();
		HttpEntity<String> requestEntity;
		requestEntity = new HttpEntity<String>(propertiesToSet, requestHeaders);
		final ResponseEntity<String> output = restClient.exchange(url, HttpMethod.POST, requestEntity, String.class);
		final HttpStatus statusCode = output.getStatusCode();
		return output;
	}

	public final List<String> getListNames(String UserEmail) {
		final String postURL = "http://localhost:8080/notes?email="+UserEmail;
		ResponseEntity<String> response = ExecuteGetRequest(postURL, requestHeaders);
		List<String> retListName = new ArrayList<String>();
		if (response == null) {
			System.out.println("Rest API to create note failed!!!!");
		} else {
			//JSONObject retJSON = new JSONObject(response.getBody());
			JSONArray retJSONArr = new JSONArray(response.getBody());
			for (int i = 0; i < retJSONArr.length(); i++) {
				retListName.add(retJSONArr.getJSONObject(i).getString("name"));
			}
		}
		return retListName;
	}

	public ResponseEntity<String> ExecuteGetRequest(String url, HttpHeaders requestHeaders) {
		RestTemplate restClient = new RestTemplate();
		HttpEntity requestEntity;
		requestEntity = new HttpEntity(null,requestHeaders);
		final ResponseEntity<String> output = restClient.exchange(url, HttpMethod.GET, requestEntity, String.class);
		final HttpStatus statusCode = output.getStatusCode();
		return output;
	}
	public void updateEmail(String NoteId,List<String> UserEmail){
		final String postURL = "http://localhost:8080/note/"+NoteId;
		JSONObject noteJSON = new JSONObject();
		JSONArray noteArrayJSON = new JSONArray();
		for(String x:UserEmail) {
			noteArrayJSON.put(x);
		}
		noteJSON.put("email", noteArrayJSON);
		ResponseEntity<String> response = ExecutePutRequest(postURL, noteJSON.toString());
		if (response == null) {
			System.out.println("Rest API to Update note failed!!!!");
		} else {
			System.out.println("noteId value:"+response.getBody());
		}
		
	}
	public ResponseEntity<String> ExecutePutRequest(String url, String propertiesToSet) {
		RestTemplate restClient = new RestTemplate();
		HttpEntity<String> requestEntity;
		requestEntity = new HttpEntity<String>(propertiesToSet, requestHeaders);
		final ResponseEntity<String> output = restClient.exchange(url, HttpMethod.PUT, requestEntity, String.class);
		final HttpStatus statusCode = output.getStatusCode();
		return output;
	}
	public void deleteNote(String NoteId) {
		final String postURL = "http://localhost:8080/note/"+NoteId;
		ResponseEntity<String> response = ExecuteDeleteRequest(postURL, requestHeaders);
		if (response == null) {
			System.out.println("Rest API to create note failed!!!!");
		}
	}
	public ResponseEntity<String> ExecuteDeleteRequest(String url, HttpHeaders requestHeaders) {
		RestTemplate restClient = new RestTemplate();
		HttpEntity requestEntity;
		requestEntity = new HttpEntity(null,requestHeaders);
		final ResponseEntity<String> output = restClient.exchange(url, HttpMethod.DELETE, requestEntity, String.class);
		final HttpStatus statusCode = output.getStatusCode();
		return output;
	}
}
