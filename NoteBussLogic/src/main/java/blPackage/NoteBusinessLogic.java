package blPackage;
import java.util.List;

public class NoteBusinessLogic {
	RestUtil restUtils;

	public NoteBusinessLogic() {
		restUtils = new RestUtil();
	}

	public List<String> getListNames(String email) {
		return restUtils.getListNames(email);
	}

	public String createNewNote(String ListName, List<String> UserEmail) {

		return restUtils.createNewNote(ListName, UserEmail);
	}

	public void updateEmail(String NoteId, List<String> UserEmail) {

		restUtils.updateEmail(NoteId, UserEmail);
	}

	public void deleteNote(String NoteId) {

		restUtils.deleteNote(NoteId);
	}
}
