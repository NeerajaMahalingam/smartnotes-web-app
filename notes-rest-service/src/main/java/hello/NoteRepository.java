package hello;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

import com.mongodb.BasicDBObject;

public interface NoteRepository extends MongoRepository<Note, String> {
	List<Note> findByEmail(@Param("name") String name);
	@Override
	void delete(String id);
	
}
