package hello;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

public interface PeopleRepository extends MongoRepository<People, String>{
	People findByEmail(@Param("name") String name);
}
