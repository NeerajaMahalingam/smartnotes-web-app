package hello;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

@RestController
public class AppController {

	@Autowired
	NoteRepository noteRepository;
	
	@Autowired
	PeopleRepository peopleRepository;

	@RequestMapping(value = "/notes", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<Note> getNotes(@RequestParam(value = "email") String email) {
		return noteRepository.findByEmail(email);
	}

	@RequestMapping(value = "/note", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public String createNote(@RequestBody Note note) {
		return noteRepository.save(note).getNoteId();
	}

	@RequestMapping(value = "/people", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public String createPeople(@RequestBody People people) {
		People getPeople = peopleRepository.findByEmail(people.getEmail());
		if (getPeople == null) {
			return peopleRepository.save(people).getPeopleId();
		}
		return null;
	}
	
	@RequestMapping(value = "/people/all", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<People> getPeople() {
		List<People> allPeople = peopleRepository.findAll();
		return allPeople;
	}

	@RequestMapping(value = "/note/{noteId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public void deleteNote(@PathVariable String noteId) {
		noteRepository.delete(noteId);
	}

	@RequestMapping(value = "/note/{noteId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Note updateNote(@PathVariable String noteId, @RequestBody Note note) {
		Note updateNote = noteRepository.findOne(noteId);
		if (note.getName() != null) {
			updateNote.setName(note.getName());
		}
		if (note.getEmail() != null) {
			List<String> email = updateNote.getEmail();
			email.addAll(note.getEmail());
			updateNote.setEmail(email);
		}
		if (note.getItem() != null) {
			List<String> item = updateNote.getItem();
			item.addAll(note.getItem());
			updateNote.setItem(item);
		}
		noteRepository.save(updateNote);
		return updateNote;
	}

	@RequestMapping(value = "/note/{noteId}/deletemail", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Note deleteMail(@PathVariable String noteId, @RequestBody Note note) {
		int cnt = 0;
		Note updateNote = noteRepository.findOne(noteId);
		List<String> tempMail = updateNote.getEmail();
		for (String getEmail : tempMail) {
			if (getEmail.equals(note.getEmail().get(0))) {
				break;
			}
			cnt++;
		}
		tempMail.remove(cnt);
		updateNote.setEmail(tempMail);
		noteRepository.save(updateNote);
		return updateNote;
	}

	@RequestMapping(value = "/note/{noteId}/deleteitem", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Note deleteItem(@PathVariable String noteId, @RequestBody String item) {
		int cnt = 0;
		String finalItem = "";
		try {
			JSONObject itemJSON = new JSONObject(item);
			finalItem = itemJSON.get("item").toString();
			System.out.println("finalItem" + finalItem);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Note updateNote = noteRepository.findOne(noteId);
		List<String> tempItem = updateNote.getItem();
		for (String getItem : tempItem) {
			if (getItem.equals(finalItem)) {
				break;
			}
			cnt++;
		}
		tempItem.remove(cnt);
		updateNote.setItem(tempItem);
		noteRepository.save(updateNote);
		return updateNote;
	}

	@RequestMapping(value = "/note/{noteId}/item", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Note updateItem(@PathVariable String noteId, @RequestBody String item) {
		String tempItem = "";
		try {
			JSONObject itemJSON = new JSONObject(item);
			tempItem = itemJSON.get("item").toString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Note updateItem = noteRepository.findOne(noteId);
		if (updateItem.getItem() != null) {
			List<String> allItems = updateItem.getItem();
			allItems.add(tempItem);
			updateItem.setItem(allItems);
		} else {
			List<String> allItems = new ArrayList<String>();
			allItems.add(tempItem);
			updateItem.setItem(allItems);
		}
		noteRepository.save(updateItem);
		return updateItem;
	}

	@RequestMapping(value = "/note", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Note getNoteDetails(@RequestParam(value = "noteId") String noteId) {
		return noteRepository.findOne(noteId);
	}

}
